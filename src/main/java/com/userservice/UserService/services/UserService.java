package com.userservice.UserService.services;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import com.userservice.UserService.dots.UserDTO;
import com.userservice.UserService.dots.OrderDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired
    private UserRepository repository;


    public List<UserDTO> getAllUsers() {
        LOGGER.info("/============= ENTER INFO getUserService in UserService ===========/");
        List<UserDTO> users = null;
        try {
            users = repository.findAll() //UserEntity
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId().toString(),
                            userEntity.getName(),
                            userEntity.getAge()
                            )).collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.warn("/********** Exception in UserService -> getAllUsers()" + e);
        }
        return users;
    }

    public List<OrderDTO> getOrdersByUserId(Long id) {
        List<OrderDTO> orders = null;
        try {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/" + id),
                    List.class);
        }catch (Exception e) {LOGGER.warn("/************** Exception is UserService -> getOrderByUserId()" + e);}
        return orders;
    }

    public boolean createUser(UserDTO user){
        try{
            UserEntity userEntity = new UserEntity();
            userEntity.setId(userEntity.getId());
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            repository.save(userEntity);
            return true;

        }catch (Exception ex){
            LOGGER.warn("Error in UserService" +ex);
        }
        return false;
    }

    public boolean updateUser(UserDTO user, Long id) {
        try {
            UserEntity userEntity = new UserEntity();
            userEntity.setId(id);
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            repository.save(userEntity);
            return true;

        } catch (Exception ex) {
            LOGGER.warn("Error in UserService" + ex);
        }
        return false;
    }


}