package com.userservice.UserService.controllers;

import com.userservice.UserService.dots.OrderDTO;
import com.userservice.UserService.dots.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    //localhost:8082/api/user/getAll
    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){
        return userService.getOrdersByUserId(id);
    }

    @PostMapping("/createUser")
    public boolean createUser(@RequestBody UserDTO user) {
        return userService.createUser(user);
    }

    @PutMapping("/updateUser/{id}")
    public boolean updateUser(@RequestBody UserDTO user, @PathVariable final Long id) {
        return userService.updateUser(user, id);
    }

}
